from pydantic import BaseModel, Field


class QueryModel(BaseModel):
    query: str = Field(..., description='Sparql Query')

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "query": "select * where {?s ?p ?o} limit 100",
                }
            ]
        }
    }
