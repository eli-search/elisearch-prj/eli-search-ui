import argparse
import logging
import os
from datetime import datetime

import aiohttp
import pytz
import toml
import math
import uvicorn
from fastapi import FastAPI, HTTPException, Response
from starlette.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

# from eli_search_api.models import QueryModel

from .lib_cfg import config

dir_path = os.path.dirname(os.path.realpath(__file__))


VERSION = 1
START_TIME = datetime.now(pytz.utc)

logger = logging.getLogger(__name__)
logger.setLevel(logging.getLevelName('INFO'))
logger.addHandler(logging.StreamHandler())

tags_metadata = [
    {
        "name": "sparql",
        "description": "execute sparql query and retrieve results"
    }

]

# ################################################# SETUP AND ARGUMENT PARSING
# ############################################################################


app = FastAPI(title="EliSearchDemo", root_path=config.key('proxy_prefix'), openapi_tags=tags_metadata)

# Server config
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def status_get(start_time, version):
    now = datetime.now(pytz.utc)
    delta = now - start_time
    delta_s = math.floor(delta.total_seconds())
    return {
        'all_systems': 'nominal',
        'id': __name__,
        'timestamp': str(now),
        'online_since': str(start_time),
        'online_for_seconds': delta_s,
        'api_version': version,
    }

# ############################################################## SERVER ROUTES
# ############################################################################
#


@app.get("/status")
def root():
    return status_get(START_TIME, VERSION)


@app.get("/sparql", tags=["sparql"])
async def query(query: str):
    sparql_endpoint = config.key('sparql_endpoint')
    logger.info('Starting async query, accessing endpoint %s', sparql_endpoint)

    if sparql_endpoint == '':
        logger.error('No sparql endpoint defined, server error')
        raise HTTPException(status_code=500, detail="No sparql endpoint configured")

    params = {
        'query': query,
        # Virtuoso specific
        # 'format': "application/sparql-results+json",
    }

    headers = {
        'Accept': "application/sparql-results+json",
    }

    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.post(sparql_endpoint, data=params, ) as resp:
            logger.info('Sparql endpoint status response: %s', resp.status)
            if resp.status != 200:
                logger.error('Endpoint returned error')
                logger.error(await resp.text())
                raise HTTPException(status_code=500, detail="Error in sparql endpoint response, see logs")

            logger.info("Response received, content-length: %s, content-type: %s",
                resp.headers.get('content-length'),
                resp.headers.get('content-type'))
            data = await resp.text()
            # Custom response, to avoid serializing ops on JSON, just passing through
            return Response(content=data, media_type="application/json")

app.mount("/", StaticFiles(directory="./static"), name="static")


# #################################################################### STARTUP
# ############################################################################
def main():
    parser = argparse.ArgumentParser(description='Matching server process')
    parser.add_argument('--config', dest='config', help='config file', default=None)
    parser.add_argument('--debug', dest='debug', action='store_true', default=False, help='Debug mode')
    args = parser.parse_args()

    # XXX: Lambda is a hack : toml expects a callable
    if args.config:
        t_config = toml.load(['config_default.toml', args.config])
    else:
        t_config = toml.load('config_default.toml')

    config.merge(t_config)

    if args.debug:
        logger.setLevel(logging.getLevelName('DEBUG'))
        logger.debug('Debug activated')
        config.set('log_level', 'debug')
        config.set(['server', 'log_level'], 'debug')
        logger.debug('Arguments: %s', args)
        config.dump(logger)
        logger.debug('config: %s', toml.dumps(config._config))

        uvicorn.run(
            "eli_search_api.main:app",
            reload=True,
            **config.key('server')
        )
    uvicorn.run(
        app,
        **config.key('server')
    )


if __name__ == "__main__":
    main()
