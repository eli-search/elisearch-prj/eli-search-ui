import { EnvConfig } from './environement-config';

export const environment: EnvConfig = {
  production: false,
  enableDevToolRedux: true,
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules'],
  },
  configMetadataUri: 'assets/country-metadata.json',
  piwikKey: 'a2f17ed3-8465-4eae-87e1-47cdde67459e',
  featureMetadataUri: 'assets/feature-metadata.json',
};
