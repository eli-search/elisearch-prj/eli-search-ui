import { EnvConfig } from './environement-config';

export const environment: EnvConfig = {
  production: true,
  enableDevToolRedux: false,
  envDynamicConfig: {
    uri: 'assets/env-json-config.json',
    deepMerge: true,
    merge: ['modules'],
  },
  configMetadataUri: 'country-metadata.json',
  piwikKey: 'd3d8337c-61c2-490b-8c44-4a82a83742eb',
  featureMetadataUri: 'assets/feature-metadata.json',
};
