import { EuiEnvConfig } from '@eui/core';

export interface EnvConfig extends EuiEnvConfig {
  production: boolean;
  configMetadataUri: string;
  piwikKey: string;
  featureMetadataUri: string;
}
