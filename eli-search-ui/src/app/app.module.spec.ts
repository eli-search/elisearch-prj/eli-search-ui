import { TestBed } from '@angular/core/testing';
import { AppModule } from './app.module';
import { AppStarterService } from './app-starter.service';
import { of } from 'rxjs';

describe('AppModule', () => {
  let appStarterService: AppStarterService;

  beforeAll(() => {
    TestBed.resetTestingModule();
  });

  beforeEach(async () => {
    const appStarterServiceMock = {
      start: jasmine.createSpy('start').and.returnValue(of(null)),
    };
    await TestBed.configureTestingModule({
      imports: [AppModule],
      providers: [{ provide: AppStarterService, useValue: appStarterServiceMock }],
    }).compileComponents();

    appStarterService = TestBed.inject(AppStarterService);
  });

  it('should call start method of AppStarterService', () => {
    expect(appStarterService.start).toHaveBeenCalled();
  });
});
