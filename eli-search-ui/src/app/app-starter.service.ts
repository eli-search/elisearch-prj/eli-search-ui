import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG_TOKEN, UserService, I18nService, EuiAppConfig, UserDetails, UserPreferences } from '@eui/core';
import { Observable, of, zip, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ConfigurationService } from './services/configuration.service';

@Injectable({
  providedIn: 'root',
})
export class AppStarterService {
  defaultUserPreferences: UserPreferences | undefined;

  constructor(
    protected userService: UserService,
    protected i18nService: I18nService,
    protected configService: ConfigurationService,
    @Inject(CONFIG_TOKEN) private config: EuiAppConfig,
    protected http: HttpClient,
  ) {}

  // eslint-disable-next-line
  start(): Observable<any> {
    return forkJoin({
      user: this.initUserService().pipe(
        switchMap((userStatus) => {
          console.log(userStatus);
          return this.i18nService.init();
        }),
      ),
      config: this.configService.loadConfig(),
      configMetadata: this.configService.loadMetadataConfig(),
      featureMetadata: this.configService.loadFeatureMetadata(),
    });
  }

  /**
   * Fetches user details,
   * create user: UserState object
   * then initialise to the UserService on run time
   */
  // eslint-disable-next-line
  initUserService(): Observable<any> {
    return zip(this.fetchUserDetails()).pipe(switchMap(([userDetails]) => this.userService.init(userDetails)));
  }

  /**
   * Fetches user details
   */
  private fetchUserDetails(): Observable<UserDetails> {
    // const url = this.config.modules.your_custom_module.your_custom_endpoint
    // const moduleCoreApi = this.config.modules?.['core'];
    // const url = `${moduleCoreApi?.['base']}${moduleCoreApi?.['userDetails']}`;
    // const user = { userId: 'anonymous' };

    // Completely disable call to mock API
    return of({
      userId: 'None',
      firstName: 'None',
      lastName: 'None',
      email: 'None',
    });
  }
}
