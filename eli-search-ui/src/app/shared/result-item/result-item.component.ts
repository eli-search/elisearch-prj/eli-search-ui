import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { formatStringDate } from '@shared/helpers/date.helpers';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiSelectModule } from '@eui/components/eui-select';
import { isDebug } from '@shared/helpers/environment.helpers';

@Component({
  selector: 'elis-result-item',
  standalone: true,
  imports: [CommonModule, TranslateModule, EuiButtonModule, EuiSelectModule],
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.scss'],
})
export class ResultItemComponent {
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  @Input() result: any;
  @Input() columns: string[];
  @Input() debug = isDebug();

  DATE_SCHEMA = 'http://www.w3.org/2001/XMLSchema#date';
  COUNTRY_URI_PREFIX = 'http://publications.europa.eu/resource/authority/country/';
  RELATION_URI_PREFIX = 'http://data.europa.eu/eli/ontology#';
  ELI_PREFIX = 'eli:';

  constructor(private translateService: TranslateService) {}

  getValue(field: string, fallbackField?: string): string {
    const resultItem = this.result[field];
    if (resultItem && resultItem.value) {
      const resultItem = this.result[field];
      if (resultItem.datatype && resultItem.datatype === this.DATE_SCHEMA) {
        return formatStringDate(resultItem.value);
      }
      if (resultItem.value.indexOf(this.COUNTRY_URI_PREFIX) >= 0) {
        const country = resultItem.value.replace(this.COUNTRY_URI_PREFIX, '');
        return this.translateService.instant('elis.country.' + country);
      }
      if (resultItem.value.indexOf(this.RELATION_URI_PREFIX) >= 0) {
        return this.ELI_PREFIX + resultItem.value.replace(this.RELATION_URI_PREFIX, '');
      }
      return resultItem.value;
    } else if (fallbackField) {
      return this.getValue(fallbackField);
    } else {
      return `-`;
    }
  }
}
