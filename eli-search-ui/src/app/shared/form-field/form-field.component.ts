import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TranslateModule } from '@ngx-translate/core';
import { EuiIconModule } from '@eui/components/eui-icon';

@Component({
  selector: 'elis-form-field',
  standalone: true,
  imports: [CommonModule, TranslateModule, EuiIconModule],
  templateUrl: './form-field.component.html',
  styleUrl: './form-field.component.scss',
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          height: '*',
          opacity: 1,
          visibility: '*',
        }),
      ),
      state(
        'out',
        style({
          height: '0px',
          opacity: 0,
          padding: '0 10px',
          visibility: 'hidden',
        }),
      ),
      transition('in <=> out', animate('300ms ease-in-out')),
    ]),
  ],
})
export class FormFieldComponent {
  @Input() fieldName: string;
  @Input() tipsText?: string; // optional
  @Input() expandable = true;
  isVisible = true;

  toggleDisplay() {
    this.isVisible = !this.isVisible;
  }
}
