// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Common 3rd party module used accross the app
import { TranslateModule } from '@ngx-translate/core';

// eUI specific modules - commonly used accross the app - prefered way
// keep here to avoid imports within features modules
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiIconModule } from '@eui/components/eui-icon';
import { EuiUserProfileModule } from '@eui/components/eui-user-profile';
import { EuiLanguageSelectorModule } from '@eui/components/eui-language-selector';
import { EuiButtonModule } from '@eui/components/eui-button';
import { EuiInputCheckboxModule } from '@eui/components/eui-input-checkbox';
import { EuiLabelModule } from '@eui/components/eui-label';

import { EuiBreadcrumbModule } from '@eui/components/eui-breadcrumb';
import { EuiPaginatorModule } from '@eui/components/eui-paginator';
import { EuiSkeletonModule } from '@eui/components/eui-skeleton';

// import ALL eUI components
// import { EuiAllModule } from '@eui/components';
import { FormFieldComponent } from './form-field/form-field.component';
import { ResultItemComponent } from './result-item/result-item.component';

import { EclAllModule } from '@eui/ecl';
import { DatePipe } from '@shared/pipes/date.pipe';
import { ElisDateFormatDirective } from '@shared/helpers/date.helpers';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';
import { SearchSectionComponent } from '@shared/search-section/search-section.component';
import { ValidationMessageComponent } from '@shared/validation-message/validation-message.component';
import { MetadataDetailsLinkComponent } from '@shared/metadata-details-link/metadata-details-link.component';
import { InfiniteScrollDirective } from '@shared/infinite-scroll/infinite-scroll.directive';
import { TreeNodeTitlePipe } from '@shared/pipes/tree-node-title.pipe';
import { FacetLabelPipe } from '@shared/pipes/facet-label.pipe';
import { ExpandableSectionComponent } from '@shared/expandable-section/expandable-section.component';

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,

  TranslateModule,

  // put here commonly used eUI modules components throughout the application
  EuiPageModule,
  EuiIconModule,
  EuiUserProfileModule,
  EuiLanguageSelectorModule,
  EuiButtonModule,

  EuiInputCheckboxModule,
  EuiLabelModule,
  EuiBreadcrumbModule,
  EuiPaginatorModule,
  EuiSkeletonModule,
  EuiChipListModule,
  EuiChipModule,

  // in case of you really want to import all eUI components, see commented imports above
  // EuiAllModule,
  FormFieldComponent,
  ResultItemComponent,
  EclAllModule,
  DatePipe,
  TreeNodeTitlePipe,
  ElisDateFormatDirective,
  InfiniteScrollDirective,
  ValidationMessageComponent,
  SearchSectionComponent,
  MetadataDetailsLinkComponent,
  FacetLabelPipe,
  ExpandableSectionComponent,
];

@NgModule({
  imports: [...MODULES],
  declarations: [],
  exports: [...MODULES],
})
export class SharedModule {}
