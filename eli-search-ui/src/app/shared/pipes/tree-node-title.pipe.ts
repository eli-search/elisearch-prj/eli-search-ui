import { Pipe, PipeTransform } from '@angular/core';
import { KeyValue } from '@angular/common';
import { Constants } from '@shared/constants/constants';
import { ELIS_DATE_FORMAT, formatDate } from '@shared/helpers/date.helpers';
import { SortingKey } from '../../features/eliResult/eliResult.component';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'treeNodeTitle',
  pure: true,
  standalone: true,
})
export class TreeNodeTitlePipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  /* eslint-disable-next-line  @typescript-eslint/no-explicit-any */
  transform(value: KeyValue<string, any>, sortingKey: SortingKey): any {
    return this.getDisplayValue(value.key, sortingKey) + ' (' + this.countItemsForKey(value) + ')';
  }

  private getDisplayValue(key: string, sortingKey: SortingKey): string {
    if (sortingKey.field === Constants.COUNTRY) {
      return key + ' - ' + this.translateService.instant('elis.country.' + key);
    }
    if (sortingKey.field === Constants.DATE_PUBLICATION || sortingKey.field === Constants.SEARCH_DATE) {
      if (sortingKey.level == 1) {
        return formatDate(ELIS_DATE_FORMAT.display.year, key);
      }
      if (sortingKey.level == 2) {
        return formatDate(ELIS_DATE_FORMAT.display.monthA11yLabel, key);
      }
      return formatDate(ELIS_DATE_FORMAT.display.monthYearLabel, key);
    }
    return key;
  }

  /* eslint-disable-next-line  @typescript-eslint/no-explicit-any */
  countItemsForKey(data: KeyValue<string, any>) {
    const nestedData = data.value;
    let count = 0;
    // If last level return the length
    if (Array.isArray(nestedData)) {
      return nestedData.length;
    }

    // Recursively traverse the nested structure
    for (const [subKey, value] of nestedData.entries()) {
      if (Array.isArray(value)) {
        // Count the items in the current array
        count += value.length;
      } else if (typeof value === 'object') {
        // Recursively count for nested objects
        count += this.countItemsForKey({ key: subKey, value });
      }
    }

    return count;
  }
}
