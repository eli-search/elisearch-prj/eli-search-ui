import { formatStringDate } from '@shared/helpers/date.helpers';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date',
  pure: false,
  standalone: true,
})
export class DatePipe implements PipeTransform {
  transform(date: string): string {
    return formatStringDate(date);
  }
}
