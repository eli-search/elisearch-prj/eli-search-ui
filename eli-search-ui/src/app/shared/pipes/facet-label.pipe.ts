import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { getFacetLabel } from '@shared/helpers/facet.helper';

@Pipe({
  name: 'facetLabel',
  pure: true,
  standalone: true,
})
export class FacetLabelPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}

  /* eslint-disable-next-line  @typescript-eslint/no-explicit-any */
  transform(key: string, category: string): any {
    return getFacetLabel(this.translateService, category, key);
  }
}
