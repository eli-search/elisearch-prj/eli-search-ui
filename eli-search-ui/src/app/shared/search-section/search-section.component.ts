import { Component, Input } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { animate, style, transition, trigger } from '@angular/animations';
import { EuiIconModule } from '@eui/components/eui-icon';

@Component({
  selector: 'elis-search-section',
  standalone: true,
  imports: [CommonModule, TranslateModule, EuiIconModule],
  templateUrl: './search-section.component.html',
  styleUrl: './search-section.component.scss',
  animations: [
    trigger('slideInOut', [
      transition(':enter', [style({ height: '0', opacity: 0 }), animate('300ms', style({ height: '*', opacity: 1 }))]),
      transition(':leave', [animate('300ms', style({ height: '0px', opacity: 0 }))]),
    ]),
  ],
})
export class SearchSectionComponent {
  @Input() sectionName: string;
  @Input() expandable = true;
  isVisible = true;

  toggleDisplay() {
    this.isVisible = !this.isVisible;
  }
}
