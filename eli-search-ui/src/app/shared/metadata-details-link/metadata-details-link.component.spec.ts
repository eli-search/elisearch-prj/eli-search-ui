import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataDetailsLinkComponent } from './metadata-details-link.component';
import { TranslateModule } from '@ngx-translate/core';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';
import { EuiPopoverModule } from '@eui/components/eui-popover';

describe('MetadataPopoverComponent', () => {
  let component: MetadataDetailsLinkComponent;
  let fixture: ComponentFixture<MetadataDetailsLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MetadataDetailsLinkComponent, EuiPopoverModule, TranslateModule.forRoot()],
      providers: [provideHttpClient(), provideHttpClientTesting()],
    }).compileComponents();

    fixture = TestBed.createComponent(MetadataDetailsLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
