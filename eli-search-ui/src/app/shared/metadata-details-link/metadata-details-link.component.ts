import { Component, Input, signal, ViewChild } from '@angular/core';
import { EuiPopoverComponent, EuiPopoverModule } from '@eui/components/eui-popover';
import { TranslateModule } from '@ngx-translate/core';
import { NgClass, NgForOf, NgIf } from '@angular/common';
import { ConfigurationService } from '../../services/configuration.service';
import { EuiChipListModule } from '@eui/components/eui-chip-list';
import { EuiChipModule } from '@eui/components/eui-chip';

@Component({
  selector: 'elis-metadata-details-link',
  standalone: true,
  imports: [EuiPopoverModule, TranslateModule, NgIf, NgForOf, EuiChipListModule, EuiChipModule, NgClass],
  templateUrl: './metadata-details-link.component.html',
  styleUrls: ['./metadata-details-link.component.scss'],
})
export class MetadataDetailsLinkComponent {
  constructor(protected configurationService: ConfigurationService) {}

  $countries = signal<string[]>(undefined);

  @Input() marginTop = false;

  @Input() set countries(countries: string[]) {
    this.$countries.set(countries);
  }

  @ViewChild('popoverMetadata') popoverMetadata: EuiPopoverComponent;

  public openPopoverMetadata(e) {
    this.popoverMetadata.openPopover(e.target);
  }
}
