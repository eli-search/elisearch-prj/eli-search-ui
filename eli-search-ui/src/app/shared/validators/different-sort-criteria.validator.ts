import { AbstractControl, ValidatorFn } from '@angular/forms';

export function differentSortCriteriaValidator(value: string): ValidatorFn {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any, @typescript-eslint/consistent-indexed-object-style
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value.split('-')[0].indexOf(value.split('-')[0]) >= 0) {
      // same sort field cannot be selected for primary and secondary
      return { sameCriteria: true };
    }

    // Criteria is valid
    return null;
  };
}
