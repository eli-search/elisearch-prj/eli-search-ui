import dayjs from 'dayjs';
import { AbstractControl, ValidatorFn } from '@angular/forms';

export function dateIsNotBeforeValueValidator(minDate: Date): ValidatorFn {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/consistent-indexed-object-style
  return (control: AbstractControl): { [key: string]: any } | null => {
    const currentDate = control.value;
    // Validate only if both dates are valid
    if (control.value && dayjs(currentDate).isBefore(minDate)) {
      // Control's date is before the other control's date
      return { dateIsBefore: true };
    }

    // Date is valid or fields are empty
    return null;
  };
}
