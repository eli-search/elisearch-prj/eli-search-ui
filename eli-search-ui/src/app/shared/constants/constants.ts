export class Constants {
  public static readonly DATE_SCHEMA = 'http://www.w3.org/2001/XMLSchema#date';
  public static readonly COUNTRY_URI_PREFIX = 'http://publications.europa.eu/resource/authority/country/';
  public static readonly RELATION_URI_PREFIX = 'http://data.europa.eu/eli/ontology#';
  public static readonly ELI_PREFIX = 'eli:';

  public static readonly TITLE = 'title';
  public static readonly COUNTRY = 'country';
  public static readonly DATE_PUBLICATION = 'date_publication';
  public static readonly SEARCH_DATE = 'search_date';
  public static readonly RELATION = 'relation';

  public static readonly PAGE_SIZE = 1000;
  public static readonly SCROLL_DISPLAYED_ITEMS_SIZE = 1000;
  public static readonly MAX_RESULTS_SIZE = 1000;
}
