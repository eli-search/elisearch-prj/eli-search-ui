import { Constants } from '@shared/constants/constants';
import { TranslateService } from '@ngx-translate/core';

export function getFacetLabel(translateService: TranslateService, category: string, key: string) {
  if (category == Constants.COUNTRY) {
    return key + ' - ' + translateService.instant('elis.country.' + key);
  } else {
    return key;
  }
}
