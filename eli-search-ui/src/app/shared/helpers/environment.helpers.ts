export function isDebug(): boolean {
  return (
    window.location.hostname !== 'elisearch.development.op-tech.publications.europa.eu' ||
    localStorage.getItem('elis-debug') === 'true'
  );
}
