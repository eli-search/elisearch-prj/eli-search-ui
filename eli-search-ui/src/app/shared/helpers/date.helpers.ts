import dayjs from 'dayjs';
import { Directive } from '@angular/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';

export function formatStringDate(dateString?: string): string | undefined {
  if (!dateString) return;
  return dayjs(dateString, 'yyyy-MM-dd').format('DD/MM/YYYY');
}

export function formatDate(type: string, dateString?: string): string | undefined {
  if (!dateString) return;
  return dayjs(dateString, 'yyyy-MM-dd').format(type);
}

export function parseStringToDate(dateString?: string): Date | undefined {
  if (!dateString) return;
  return dayjs(dateString, 'yyyy-MM-dd').toDate();
}

export const ELIS_DATE_FORMAT = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
    year: 'YYYY',
    month: 'MM',
    monthA11yLabel: 'MMMM',
    yearMonth: 'YYYY-MM',
    yearMonthDay: 'YYYY-MM-DD',
  },
};

@Directive({
  selector: '[elisDateFormat]',
  standalone: true,
  providers: [{ provide: MAT_DATE_FORMATS, useValue: ELIS_DATE_FORMAT }],
})
export class ElisDateFormatDirective {}
