// Preserve original property order
import { KeyValue } from '@angular/common';

// eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any
export function originalOrder(a: KeyValue<string, any>, b: KeyValue<string, any>): number {
  return 0;
}
