export interface SearchParams {
  // actual search params to consider for query builder
  countries?: string[];

  NatActURI?: string;

  dateType?: string;
  dateFrom?: string;
  dateTo?: string;

  relations?: string;
  EUActURI?: string;
  EUCelex?: string;
  EUYear?: string;

  // internal properties for form
  allCountries?: boolean;
  countriesGroup: {
    LUX?: boolean;
    ESP?: boolean;
    PRT?: boolean;
  };
  relation?: string;
  EUCriteriaType?: string;
  dateRange?: string;
  specificDate?: string;
  // pagination
  page?: number;
}
