export interface GroupingModel {
  primary: string;
  secondary: string;
}
