export interface ConfigParams {
  sparqlEndpoint: string;
  defaultQuery: string;
  cellarFederatedQuery: string;
  // new query templates
  yearQuery: string;
  euLegalActQuery: string;
  relationshipQuery: string;
  euLegalActYearRelationshipQuery: string;
  euCelexRelationshipQuery: string;
  nationalUriQuery: string;
}
