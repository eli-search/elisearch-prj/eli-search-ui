export interface FeatureMetadata {
  nationalUri?: string[];
  date?: string[];
  textSearch?: string[];
  nationalVoc?: string[];
  relation?: string[];
}
