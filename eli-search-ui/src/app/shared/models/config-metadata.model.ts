/* eslint-disable  @typescript-eslint/consistent-indexed-object-style */
export interface ConfigMetadata {
  [country: string]: string[];
}
