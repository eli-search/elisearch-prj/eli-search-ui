import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandableSectionComponent } from './expandable-section.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FormFieldComponent', () => {
  let component: ExpandableSectionComponent;
  let fixture: ComponentFixture<ExpandableSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ExpandableSectionComponent, BrowserAnimationsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpandableSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
