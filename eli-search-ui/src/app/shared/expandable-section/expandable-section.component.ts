import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TranslateModule } from '@ngx-translate/core';
import { EuiIconModule } from '@eui/components/eui-icon';

@Component({
  selector: 'elis-expandable-section',
  standalone: true,
  imports: [CommonModule, TranslateModule, EuiIconModule],
  templateUrl: './expandable-section.component.html',
  styleUrl: './expandable-section.component.scss',
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          height: '*',
          opacity: 1,
          visibility: '*',
        }),
      ),
      state(
        'out',
        style({
          height: '0px',
          opacity: 0,
          padding: '0 10px',
          visibility: 'hidden',
        }),
      ),
      transition('in <=> out', animate('300ms ease-in-out')),
    ]),
  ],
})
export class ExpandableSectionComponent {
  @Input() title: string;
  @Input() expandable = true;
  @Input() isExpanded = true;

  toggleDisplay() {
    this.isExpanded = !this.isExpanded;
  }
}
