import { Component, Input } from '@angular/core';
import { EuiFeedbackMessageModule } from '@eui/components/eui-feedback-message';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'elis-validation-message',
  standalone: true,
  imports: [EuiFeedbackMessageModule, TranslateModule],
  templateUrl: './validation-message.component.html',
  styleUrl: './validation-message.component.scss',
})
export class ValidationMessageComponent {
  @Input() validationMessageKey: string;
}
