import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AppStarterService } from './app-starter.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CachePreventionInterceptor, CsrfPreventionInterceptor } from '@eui/core';
import { NgxPiwikProModule, NgxPiwikProRouterModule } from '@piwikpro/ngx-piwik-pro';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    AppRoutingModule,
    NgxPiwikProModule.forRoot(environment.piwikKey, 'https://analytics.webanalytics.op.europa.eu/containers'),
    NgxPiwikProRouterModule,
  ],
  providers: [
    AppStarterService,
    {
      provide: APP_INITIALIZER,
      useFactory: (appStarterService: AppStarterService) => () =>
        new Promise<void>((resolve) => {
          appStarterService.start().subscribe(() => resolve());
        }),
      deps: [AppStarterService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CachePreventionInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CsrfPreventionInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
