import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, of, tap } from 'rxjs';
import { ConfigParams } from '@shared/models/config-params.model';
import { HttpClient } from '@angular/common/http';
import { ConfigMetadata } from '@shared/models/config-metadata.model';
import { environment } from '../../environments/environment';
import { FeatureMetadata } from '@shared/models/feature-metadata.model';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {
  private configSubject: BehaviorSubject<ConfigParams> = new BehaviorSubject<ConfigParams>(null);
  private configMetadataSubject: BehaviorSubject<ConfigMetadata> = new BehaviorSubject<ConfigMetadata>(null);
  private featureMetadataSubject: BehaviorSubject<FeatureMetadata> = new BehaviorSubject<FeatureMetadata>(null);
  config$ = this.configSubject.asObservable();
  configMetadata$ = this.configMetadataSubject.asObservable();

  constructor(private http: HttpClient) {}

  loadConfig(): Observable<ConfigParams> {
    return (
      this.http
        //.get<ConfigParams>('assets/ui-config-virtuoso.json')
        .get<ConfigParams>('assets/ui-config-default.json')
        .pipe(tap((config) => this.configSubject.next(config)))
    );
  }

  updateConfig(newConfig: ConfigParams): void {
    this.configSubject.next(newConfig);
  }

  getConfig(): ConfigParams {
    return this.configSubject.getValue();
  }

  loadMetadataConfig(): Observable<ConfigMetadata> {
    return this.http.get<ConfigMetadata>(environment.configMetadataUri).pipe(
      tap((config) => {
        // clean metadata for display
        const keys = Object.keys(config);
        keys.forEach((key) => {
          config[key] = config[key]
            .map((metadata) => metadata.replace('http://data.europa.eu/eli/ontology#', 'eli:'))
            .filter((metadata) => metadata.startsWith('eli:')) // keeps only eli metadata
            //.filter((metadata) => config.used?.indexOf(metadata) >= 0)
            .sort((a, b) => (a > b ? 1 : -1));
        });
        this.configMetadataSubject.next(config);
      }),
      catchError((error) => {
        console.log('Failed to retrieve metadata: ', error);
        return of({});
      }),
    );
  }

  getMetadataConfig(): ConfigMetadata {
    return this.configMetadataSubject.getValue();
  }

  loadFeatureMetadata(): Observable<FeatureMetadata> {
    return this.http.get<ConfigMetadata>(environment.featureMetadataUri).pipe(
      tap((config) => {
        this.featureMetadataSubject.next(config);
      }),
      catchError((error) => {
        console.log('Failed to retrieve feature metadata: ', error);
        return of({});
      }),
    );
  }

  getFeatureMetadata(): FeatureMetadata {
    return this.featureMetadataSubject.getValue();
  }
}
