import { Injectable } from '@angular/core';
import { SortingKey } from '../features/eliResult/eliResult.component';

@Injectable({
  providedIn: 'root',
})
export class ResultService {
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  private data: any;
  private pagingData: any;

  private primarySK: SortingKey;
  private secondarySK: SortingKey;
  private tertiarySK: SortingKey;

  setData(data: any) {
    console.log('Received data', data);
    this.data = data;
  }

  setPagingData(data: any) {
    console.log('Paging data', data);
    this.pagingData = data;
  }

  getData(): any {
    return this.data;
  }

  getPagingData(): any {
    return this.pagingData;
  }

  setPrimarySortingKey(primary: SortingKey) {
    this.primarySK = primary;
  }

  setSecondarySortingKey(secondary: SortingKey) {
    this.secondarySK = secondary;
  }

  setTertiarySortingKey(tertiary: SortingKey) {
    this.tertiarySK = tertiary;
  }

  getPrimarySortingKey() {
    return this.primarySK;
  }

  getSecondarySortingKey() {
    return this.secondarySK;
  }

  getTertiarySortingKey() {
    return this.tertiarySK;
  }
}
