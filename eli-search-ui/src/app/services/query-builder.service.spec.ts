import { TestBed } from '@angular/core/testing';
import { QueryBuilderService } from './query-builder.service';
import { provideHttpClient } from '@angular/common/http';
import { provideHttpClientTesting } from '@angular/common/http/testing';

describe('QueryBuilderService', () => {
  let service: QueryBuilderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()],
    });
    service = TestBed.inject(QueryBuilderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
