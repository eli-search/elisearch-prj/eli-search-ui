/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, of } from 'rxjs';
import { ConfigurationService } from './configuration.service';
import { SearchParams } from '@shared/models/search-params.model';
import { QueryBuilderService } from './query-builder.service';
import { SortingKey } from '../features/eliResult/eliResult.component';

@Injectable({
  providedIn: 'root',
})
export class SearchService {
  private rawQuery: string;

  // Search service stores paging data, as it is responsible
  // for computing the limits and offsets.
  private pageSize = 10;
  private pageCurrent = 0;
  private pageCount = 0;
  private searchParams: SearchParams;
  private hasResults = false;
  private resultCount = 0;

  private loading = false;
  private error = false;
  private noSearchParams = false;

  private primarySK: SortingKey;
  private secondarySK: SortingKey;

  constructor(
    private http: HttpClient,
    private configService: ConfigurationService,
    private queryBuilderService: QueryBuilderService,
  ) {}

  /************************************************ Pagination & search handling
   ****************************************************************************/

  setPageSize(size: number) {
    this.pageSize = size;
    return this;
  }
  getPageSize(): number {
    return this.pageSize;
  }
  setPageCurrent(page: number) {
    this.pageCurrent = page;
    return this;
  }
  getPageCurrent(): number {
    return this.pageCurrent;
  }
  getNumberOfResultsRequested(): number {
    return (this.pageCurrent + 1) * this.pageSize;
  }
  setSearchParams(params: SearchParams) {
    this.searchParams = params;
    console.log('searchParams', params);
    return this;
  }
  getSearchParams(): SearchParams {
    return this.searchParams;
  }

  setLoading(loading: boolean) {
    this.loading = loading;
    return this;
  }
  isLoading() {
    return this.loading;
  }

  hasError() {
    return this.error;
  }

  hasNoSearchParams() {
    return this.noSearchParams;
  }

  setPrimarySortingKey(primary: SortingKey) {
    this.primarySK = primary;
    return this;
  }

  setSecondarySortingKey(secondary: SortingKey) {
    this.secondarySK = secondary;
    return this;
  }

  getPrimarySortingKey() {
    return this.primarySK;
  }

  getSecondarySortingKey() {
    return this.secondarySK;
  }

  getResultCount(): number {
    return this.resultCount;
  }

  getPagingData(): any {
    return {
      'resultCount': this.getResultCount(),
      'pageCurrent': this.getPageCurrent(),
      'pageSize': this.getPageSize(),
      'pageCount': this.pageCount,
    };
  }

  /************************************************************ Search execution
   ****************************************************************************/
  search(): Observable<any> {
    this.resetFlags();
    const query = this.queryBuilderService.buildSearchQuery(
      this.searchParams,
      this.getQueryPaging(),
      this.getQuerySorting(),
    );

    // undefined if no matching template
    if (!query) {
      this.setLoading(false);
      this.noSearchParams = true;
      return of();
    }
    return this.execute(query);
  }

  execute(query: string): Observable<any> {
    this.storeQuery(query);

    const sparqlEndpoint = this.configService.getConfig().sparqlEndpoint;
    this.error = false;
    return this.http
      .post(sparqlEndpoint, query, {
        headers: {
          'Accept': 'application/sparql-results+json',
          'Content-Type': 'application/sparql-query',
        },
      })
      .pipe(
        catchError((error) => {
          this.error = true;
          this.setLoading(false);
          console.error('Error:', error);
          return of();
        }),
      );
  }

  storeQuery(query: string) {
    this.rawQuery = query;
  }

  getQuery(): string {
    return this.rawQuery ? this.rawQuery : '(Query is empty)';
  }

  getQueryPaging(): string {
    // limit: pageSize
    // offset: current page * page size
    return `LIMIT ${this.pageSize} OFFSET ${this.pageCurrent * this.pageSize}`;
  }

  getQuerySorting(): string {
    return `ORDER BY ${this.primarySK.sortOrder}(?${this.primarySK.field}) ${this.secondarySK.sortOrder}(?${this.secondarySK.field})`;
  }

  resetFlags() {
    this.noSearchParams = false;
    this.error = false;
  }
}
