import { Injectable } from '@angular/core';
import { SearchParams } from '@shared/models/search-params.model';
import { ConfigurationService } from './configuration.service';

@Injectable({
  providedIn: 'root',
})
export class QueryBuilderService {
  constructor(private configService: ConfigurationService) {}

  buildSearchQuery(params: SearchParams, queryPaging: string, querySorting: string): string {
    let QueryObj: QueryObject = {
      values: [],
      queries: [],
      filters: [],
      services: [],
    };

    // Update query object
    QueryObj = this.checkSearchByDate(params, QueryObj);
    QueryObj = this.checkSearchByCountry(params, QueryObj);
    QueryObj = this.checkSearchByEUAct(params, QueryObj);
    QueryObj = this.checkSearchByNatAct(params, QueryObj);
    QueryObj = this.checkFederatedSearches(params, QueryObj);

    // Build query with mask & query object
    const query = this.buildQuery(this.getQueryTemplate(params), QueryObj, queryPaging, querySorting);

    return query;
  }

  getQueryTemplate(searchParams: SearchParams): string | undefined {
    /** determine the query template to use
     * 6 templates available (30/10/2024):
     * 1. Search by year
     * 2. Search by EU Legal Act
     * 3. Search by relationship
     * 4. Search by year of EU Legal Act + relationship
     * 5. Search by EU Legal Act celex + relationship
     * 6. Search by national identifier
     */

    const configParams = this.configService.getConfig();

    // case 1
    if (searchParams.dateType && (searchParams.dateFrom || searchParams.dateTo || searchParams.specificDate)) {
      return configParams.yearQuery;
    }

    // case 2
    if (searchParams.EUActURI && !searchParams.relation) {
      return configParams.euLegalActQuery;
    }

    // case 3
    if (searchParams.EUActURI && searchParams.relation) {
      return configParams.relationshipQuery;
    }

    // case 4
    if (searchParams.EUYear && searchParams.relation) {
      return configParams.euLegalActYearRelationshipQuery;
    }

    // case 5
    if (searchParams.EUCelex && searchParams.relation) {
      return configParams.euCelexRelationshipQuery;
    }

    // case 6
    if (searchParams.NatActURI) {
      return configParams.nationalUriQuery;
    }

    console.error('No matching template!');
    return undefined;
  }

  getCartesianValueList(...valueSets) {
    return valueSets.reduce(
      (acc, group) => {
        return acc.flatMap((valueSet) => group.map((val) => [...valueSet, val]));
      },
      [[]],
    );
  }

  buildQuery(mask: string, queryobj: QueryObject, queryPaging: string, querySorting: string) {
    if (!mask) {
      return undefined;
    }
    // Values

    // As there may be multiple values per key, we generate a
    // cartesian product of them (we should be *very* careful with this)
    const KeyValueList = queryobj.values.reduce((acc, i) => {
      if (!acc[i.k]) {
        acc[i.k] = [];
      }
      acc[i.k].push(i.v);
      return acc;
    }, {});

    const ValueProd = this.getCartesianValueList(...Object.values(KeyValueList));
    const ValueString = ValueProd.flatMap((group) => `( ${group.join(' ')} )`).join('\n');
    const KeyString = Object.keys(KeyValueList).join(' ');

    // Queries
    const QueryString = queryobj.queries.join('\n');

    // Filters
    const FiltersString = queryobj.filters.map((i) => `(${i})`).join(' && \n');
    const FilterString =
      FiltersString != ''
        ? `
        FILTER (
            ${FiltersString}
        )`
        : '';

    // Services
    const ServiceString = queryobj.services.length > 0 ? this.buildServiceQuery(mask, queryobj) : '';

    return mask
      .replace(/%KeyString%/, KeyString)
      .replace(/%ValueString%/, ValueString)
      .replace(/%QueryString%/, QueryString)
      .replace(/%FilterString%/, FilterString)
      .replace(/%ServiceString%/, ServiceString)
      .replace(/%SortingString%/, querySorting)
      .replace(/%PagingString%/, queryPaging);
  }

  buildServiceQuery(mask, queryobj) {
    // Filters
    const FiltersString = queryobj.services.map((i) => `(${i})`).join(' && \n');
    const FilterString =
      FiltersString != ''
        ? `
        FILTER (
            ${FiltersString}
        )`
        : '';

    return FilterString;
  }

  /******************************************************* Search feature fields
   ****************************************************************************/

  checkSearchByEUAct(params, obj) {
    /* Search by reference to EU Act
     * Radio selector is currently ignored
     * Needs to be a full and referenceble URI
     * Maybe improve to only allow ELI
     */
    if (!params.EUActURI) {
      return obj;
    }

    obj['values'].push({ 'k': '?directive ', 'v': `<${params.EUActURI}>` });
    // obj['queries'].push('?entryPoint ?relation ?directive.');
    // obj['queries'].push('?entryPoint (eli:realizes|^eli:is_realized_by)? ?work.');

    // add EU Relation type to query
    obj = this.checkEURelation(params, obj);

    return obj;
  }

  checkSearchByNatAct(params, obj) {
    /* Search by reference to National Act
     * Needs to be a full and referenceble URI
     * Maybe improve to only allow ELI
     */
    if (!params.NatActURI) {
      return obj;
    }

    obj['values'].push({ 'k': '?work ', 'v': `<${params.NatActURI}>` });

    return obj;
  }

  checkSearchByCountry(params, obj) {
    /* Search by country
     * Depends on namespace http://publications.europa.eu/resource/authority/country/
     */

    if (params.allCountries) {
      // Means we should not filter by country, at all
      obj['values'].push({ 'k': '?country', 'v': `UNDEF` });
      return obj;
    }

    for (const [country, enabled] of Object.entries(params.countriesGroup)) {
      if (enabled) {
        obj['values'].push({ 'k': '?country', 'v': `country:${country}` });
      }
    }

    return obj;
  }

  checkSearchByDate(params: SearchParams, obj) {
    if (!((params.dateFrom || params.dateTo || params.specificDate) && params.dateType)) {
      return obj;
    }

    // if specificDate is set it's a exact match search with the specificDate, else it's range search with the dateFrom and dateTo
    const startDateOperator = params.specificDate ? '=' : '>=';

    switch (params.dateType) {
      case 'validity':
        if (params.dateFrom) {
          obj['values'].push({ 'k': '?search_property', 'v': `eli:first_date_entry_in_force` });
          // obj['queries'].push('?work eli:first_date_entry_in_force ?entry_date.');
          obj['filters'].push('?entry_date ' + startDateOperator + ' ?startDate');
        }
        if (params.dateTo) {
          obj['values'].push({ 'k': '?search_property', 'v': `eli:date_no_longer_in_force` });
          // obj['queries'].push('?work eli:date_no_longer_in_force ?exit_date.');
          obj['filters'].push('?exit_date <= ?endDate');
        }
        break;
      case 'signature':
        obj['values'].push({ 'k': '?search_property', 'v': `eli:date_document` });
        // obj['queries'].push('?work eli:date_document ?search_date.');
        this.setSearchDates(params, obj, startDateOperator);
        break;
      default:
        obj['values'].push({ 'k': '?search_property', 'v': `eli:date_publication` });
        // obj['queries'].push('?work eli:date_publication ?search_date.');
        this.setSearchDates(params, obj, startDateOperator);
    }

    if (params.specificDate) {
      obj['values'].push({ 'k': '?startDate', 'v': `"${params.specificDate}"^^xsd:date` });
    }

    if (params.dateFrom) {
      obj['values'].push({ 'k': '?startDate', 'v': `"${params.dateFrom}"^^xsd:date` });
    }

    if (params.dateTo) {
      obj['values'].push({ 'k': '?endDate', 'v': `"${params.dateTo}"^^xsd:date` });
    }
    return obj;
  }

  private setSearchDates(params: SearchParams, obj, startDateOperator: string) {
    if (params.dateFrom || params.specificDate) {
      obj['filters'].push('?search_date ' + startDateOperator + ' ?startDate');
    }
    if (params.dateTo) {
      obj['filters'].push('?search_date <= ?endDate');
    }
  }

  checkFederatedSearches(params, obj) {
    // Currently only EUCelex and EUYear use federated queries to CELLAR
    // Services query only use filters at the moment, so a "naive"
    // approach is sufficient for the time being
    let addRelation = false;
    if (params.EUCelex) {
      obj['services'].push(`STR(?dir_celex) = "${params.EUCelex}"`);
      addRelation = true;
    }
    if (params.EUYear) {
      obj['services'].push(`YEAR(?wdd) = "${params.EUYear}"^^xsd:integer`);
      addRelation = true;
    }

    // add EU Relation type to query
    if (addRelation) {
      obj = this.checkEURelation(params, obj);
    }

    return obj;
  }

  checkEURelation(params, obj) {
    // for ANY: array with the 5 relationships
    // for specific relation : array with only the selected relationship
    const relations: string[] = [
      'eli:related_to',
      'eli:cites',
      'eli:transposes',
      'eli:ensures_implementation_of',
      'eli:applies',
    ];

    if (params.relation == 'ANY') {
      relations.map((rel) => obj['values'].push({ 'k': '?relation ', 'v': rel }));
    } else {
      obj['values'].push({ 'k': '?relation ', 'v': params.relation });
    }

    return obj;
  }
}

/* eslint-disable @typescript-eslint/no-explicit-any */
export interface QueryObject {
  values: any[];
  queries: any[];
  filters: any[];
  services: any[];
}
