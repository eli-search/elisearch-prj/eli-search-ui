import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { isDebug } from '@shared/helpers/environment.helpers';

@Component({
  selector: 'elis-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  debug = isDebug();
  navbarItems = [
    {
      id: 'countries_list',
      label: 'Countries list',
      url: 'https://n-lex.europa.eu/n-lex/index',
      external: true,
      class: '',
    },
    {
      id: 'multidatabase_search',
      label: 'Multi-database Search',
      url: 'https://n-lex.europa.eu/n-lex/aggregated-search',
      external: true,
      class: '',
    },
    {
      id: 'eli_based_search',
      label: 'Eli Based Search',
      url: 'screen/eliSearch',
      class: '',
    },
    {
      id: 'noneu_countries',
      label: 'Non-EU Countries',
      url: 'https://n-lex.europa.eu/n-lex/related_links/related_links',
      external: true,
      class: '',
    },
  ];
  navbarItemActive = this.navbarItems[2]; // Eli Search tab

  constructor(private router: Router) {}

  ngOnInit() {
    if (this.debug) {
      this.navbarItems.push({
        id: 'sparql_test',
        label: 'SparQL Tester',
        url: 'screen/sparqlTest',
        class: 'debug',
      });
      this.navbarItems.push({
        id: 'set_config',
        label: 'Settings',
        url: 'screen/setConfig',
        class: 'debug',
      });
    }

    window.onscroll = () => {
      const offsetTop =
        document.getElementById('elis-sticky-panel')?.offsetTop +
        (document.getElementById('eui-breadcrumb')?.offsetHeight || 0);
      if (document.body.scrollTop > offsetTop || document.documentElement.scrollTop > offsetTop) {
        document.querySelector('.elis-sticky-panel')?.classList.add('elis-fixed-position');
      } else {
        document.querySelector('.elis-sticky-panel')?.classList.remove('elis-fixed-position');
      }
    };
  }

  onClick(id: string) {
    console.log(`item clicked: ${id}`);
    const item = this.navbarItems.find((item) => item.id === id);
    if (item) {
      this.goTo(item);
    }
  }

  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  private goTo(item: any) {
    if (item.external) {
      window.open(item.url, '_self');
    } else {
      this.router.navigate([item.url]);
    }
  }
}
