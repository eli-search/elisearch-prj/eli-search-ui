import { TestBed } from '@angular/core/testing';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { CONFIG_TOKEN, EuiAppConfig, I18nService } from '@eui/core';
import { of } from 'rxjs';
import { AppStarterService } from './app-starter.service';
import { EuiServiceStatus } from '@eui/base';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import SpyObj = jasmine.SpyObj;
import { provideMockStore, MockStore } from '@ngrx/store/testing';

describe('AppStarterService', () => {
  let service: AppStarterService;
  /* eslint-disable-next-line @typescript-eslint/no-unused-vars */
  let httpMock: HttpTestingController;
  //let userServiceMock: SpyObj<UserService>;
  let i18nServiceMock: SpyObj<I18nService>;
  let configMock: EuiAppConfig;
  /* eslint-disable-next-line @typescript-eslint/no-unused-vars */
  let store: MockStore;

  beforeEach(() => {
    //userServiceMock = jasmine.createSpyObj('UserService', ['init']);
    i18nServiceMock = jasmine.createSpyObj('I18nService', ['init']);
    configMock = {
      global: {},
      modules: { core: { base: 'localhost:3000', userDetails: 'dummy' } },
    };

    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
        AppStarterService,
        //{provide: UserService, useValue: userServiceMock},
        { provide: I18nService, useValue: i18nServiceMock },
        { provide: CONFIG_TOKEN, useValue: configMock },
        provideMockStore({}),
      ],
    });

    service = TestBed.inject(AppStarterService);
    httpMock = TestBed.inject(HttpTestingController);
    store = TestBed.inject(MockStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call start method', () => {
    //userServiceMock.init.and.returnValue(of({ } as EuiServiceStatus));
    i18nServiceMock.init.and.returnValue(of({} as EuiServiceStatus));
    service.start().subscribe(() => {
      //expect(userServiceMock.init).toHaveBeenCalled();
      expect(i18nServiceMock.init).toHaveBeenCalled();
    });
  });

  // Deactivating mock API
  //it('should call initUserService method', () => {
  //    userServiceMock.init.and.returnValue(of({ } as EuiServiceStatus));
  //    service.initUserService().subscribe(() => {
  //        expect(userServiceMock.init).toHaveBeenCalled();
  //    });
  //});

  //it('should fetch user details', () => {
  //    const dummyUserDetails = {userId: 'anonymous'};
  //    service['fetchUserDetails']().subscribe(userDetails => {
  //        expect(userDetails).toEqual(dummyUserDetails);
  //    });

  //    const req = httpMock.expectOne(`${configMock.modules?.['core']['base']}${configMock.modules?.['core']['userDetails']}`);
  //    expect(req.request.method).toBe('GET');
  //    req.flush(dummyUserDetails);
  //});
});
