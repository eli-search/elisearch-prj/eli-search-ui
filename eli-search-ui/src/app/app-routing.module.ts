import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: 'screen/eliSearch', pathMatch: 'full' },
  { path: 'index.jsp', redirectTo: 'screen/eliSearch' },
  {
    path: 'screen/eliSearch',
    loadChildren: () => import('./features/eliSearch/eliSearch.module').then((m) => m.Module),
  },
  {
    path: 'screen/eliResult',
    loadChildren: () => import('./features/eliResult/eliResult.module').then((m) => m.Module),
  },
  {
    path: 'screen/eliMetadata',
    loadChildren: () => import('./features/eli-metadata/eli-metadata.module').then((m) => m.Module),
  },
  {
    path: 'screen/sparqlTest',
    loadChildren: () => import('./features/sparql-test/sparql-test.module').then((m) => m.Module),
    // canActivate: [() => of(isDebug)],
  },
  {
    path: 'screen/setConfig',
    loadChildren: () => import('./features/set-config/set-config.module').then((m) => m.Module),
    // canActivate: [() => of(isDebug)],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule {}
