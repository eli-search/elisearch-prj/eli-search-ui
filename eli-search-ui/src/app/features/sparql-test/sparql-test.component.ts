import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { ResultService } from '../../services/result.service';

@Component({
  templateUrl: './sparql-test.component.html',
  styleUrl: './sparql-test.component.scss',
})
export class SparqlTestComponent implements OnInit {
  query = '';

  constructor(
    private searchService: SearchService,
    private resultService: ResultService,
    private router: Router,
  ) {}

  ngOnInit() {
    const query = this.searchService.getQuery();
    if (query) {
      this.query = query;
    } else {
      this.query = '(no query found)';
    }
  }

  onSubmit() {
    this.searchService.execute(this.query).subscribe(
      (data) => {
        this.resultService.setData(data);
        this.router.navigate(['screen/eliResult']);
      },
      (error) => {
        console.error('Error:', error);
      },
    );
  }
}
