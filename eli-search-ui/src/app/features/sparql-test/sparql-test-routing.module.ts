import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SparqlTestComponent } from './sparql-test.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: SparqlTestComponent }])],
})
export class SparqlTestRoutingModule {}
