import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SparqlTestRoutingModule } from './sparql-test-routing.module';
import { SparqlTestComponent } from './sparql-test.component';

@NgModule({
  declarations: [SparqlTestComponent],
  imports: [SparqlTestRoutingModule, SharedModule],
})
export class Module {}
