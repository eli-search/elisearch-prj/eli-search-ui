import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { EliMetadataComponent } from './eli-metadata.component';
import { EliMetadataRoutingModule } from './eli-metadata-routing.module';

@NgModule({
  imports: [SharedModule, EliMetadataRoutingModule],
  declarations: [EliMetadataComponent],
})
export class Module {}
