import { Component, Input, OnInit, signal } from '@angular/core';
import { ConfigurationService } from '../../services/configuration.service';
import { getCountryISO2 } from '@shared/helpers/country.helpers';
import { ConfigMetadata } from '@shared/models/config-metadata.model';
import { FeatureMetadata } from '@shared/models/feature-metadata.model';

@Component({
  selector: 'elis-eli-metadata',
  templateUrl: './eli-metadata.component.html',
  styleUrl: './eli-metadata.component.scss',
})
export class EliMetadataComponent implements OnInit {
  featuresByCountry: FeatureByCountry[] = [];
  featureMetadata: FeatureMetadata;
  metadataConfig: ConfigMetadata;

  constructor(protected configurationService: ConfigurationService) {}

  ngOnInit(): void {
    this.featureMetadata = this.configurationService.getFeatureMetadata();
    this.metadataConfig = this.configurationService.getMetadataConfig();
    this.groupMetadata();
  }

  $countries = signal<string[]>(['ESP', 'LUX', 'PRT']);

  @Input() set countries(countries: string[]) {
    this.$countries.set(countries);
  }

  public getCountryISO2(countryISO3: string) {
    return getCountryISO2(countryISO3).toLowerCase();
  }

  private groupMetadata() {
    if (this.featureMetadata && this.metadataConfig) {
      for (const country of this.$countries()) {
        const features: Feature[] = [];
        const usedMetadata = new Set();
        Object.keys(this.featureMetadata).forEach((item: string) => {
          const usedMetadataAmongAvailable: string[] = this.featureMetadata[item].filter((featureMetadata) =>
            this.metadataConfig[country].includes(featureMetadata),
          );
          features.push({
            name: item,
            metadata: usedMetadataAmongAvailable,
          });

          usedMetadataAmongAvailable.forEach((item) => {
            usedMetadata.add(item);
          });
        });
        const available: string[] = this.metadataConfig[country].filter(
          (metadata) => ![...usedMetadata].includes(metadata),
        );
        this.featuresByCountry.push({ country, features, available });
      }
    }
  }
}

interface FeatureByCountry {
  country: string;
  features: Feature[];
  available: string[];
}

interface Feature {
  name: string;
  metadata: string[];
}
