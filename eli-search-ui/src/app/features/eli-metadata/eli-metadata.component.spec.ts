import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliMetadataComponent } from './eli-metadata.component';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';
import { EuiPageModule } from '@eui/components/eui-page';
import { EuiBreadcrumbModule } from '@eui/components/eui-breadcrumb';
import { TranslateModule } from '@ngx-translate/core';
import { ConfigurationService } from '../../services/configuration.service';

describe('EliMetadataComponent', () => {
  let component: EliMetadataComponent;
  let fixture: ComponentFixture<EliMetadataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EuiPageModule, EuiBreadcrumbModule, TranslateModule.forRoot()],
      declarations: [EliMetadataComponent],
      providers: [provideHttpClient(), provideHttpClientTesting(), ConfigurationService],
    }).compileComponents();

    fixture = TestBed.createComponent(EliMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
