import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EliMetadataComponent } from './eli-metadata.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: EliMetadataComponent }])],
})
export class EliMetadataRoutingModule {}
