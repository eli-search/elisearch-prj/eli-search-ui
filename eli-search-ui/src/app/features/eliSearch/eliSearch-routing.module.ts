import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EliSearchComponent } from './eliSearch.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: EliSearchComponent }])],
})
export class EliSearchRoutingModule {}
