import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { ResultService } from '../../services/result.service';
import { isDebug } from '@shared/helpers/environment.helpers';
import { EliSearchForm } from './eliSearch.form';
import { map } from 'rxjs';
import { Constants } from '@shared/constants/constants';

@Component({
  templateUrl: './eliSearch.component.html',
  styleUrls: ['./eliSearch.component.scss'],
})
export class EliSearchComponent implements OnInit {
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  results: any[];
  debug = isDebug();
  searchForm = new EliSearchForm();
  maxDate = new Date();

  constructor(
    public searchService: SearchService,
    private resultService: ResultService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.searchForm.patchAndSubscribe(this.searchService.getSearchParams());
  }

  onSubmit() {
    if (this.searchForm.valid) {
      this.searchService
        .setPageSize(Constants.PAGE_SIZE)
        .setPageCurrent(0)
        .setLoading(true)
        .setSearchParams(this.searchForm.buildPayload())
        .setPrimarySortingKey({
          field: this.searchService.getSearchParams().dateType ? 'search_date' : 'date_publication',
          fallbackField: this.searchService.getSearchParams().dateType ? 'search_date' : 'date_publication',
          sortOrder: 'desc',
        })
        .setSecondarySortingKey({
          field: 'country',
          sortOrder: 'asc',
        })
        .search()
        .pipe(
          map((data) => {
            this.searchService.setLoading(false);
            this.resultService.setData(data);
            this.resultService.setPrimarySortingKey({ ...this.searchService.getPrimarySortingKey(), level: 1 });
            this.resultService.setSecondarySortingKey({ ...this.searchService.getPrimarySortingKey(), level: 2 });
            this.resultService.setTertiarySortingKey({ ...this.searchService.getSecondarySortingKey(), level: 3 });
            this.resultService.setPagingData(this.searchService.getPagingData());
            this.router.navigate(['screen/eliResult']);
          }),
        )
        .subscribe();
    } else {
      console.log('form not valid');
    }
  }

  clearParams() {
    this.searchForm.reset();
    this.searchService.resetFlags();
  }
}
