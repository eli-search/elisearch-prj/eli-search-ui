import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SearchParams } from '@shared/models/search-params.model';
import { debounceTime, distinctUntilChanged, map, startWith } from 'rxjs';
import { parseStringToDate } from '@shared/helpers/date.helpers';
import { dateIsNotBeforeValueValidator } from '@shared/validators/date-is-not-before-value.validator';
import { requireCheckboxesToBeCheckedValidator } from '@shared/validators/require-checkboxes-to-be-checked.validator';

export class EliSearchForm extends FormGroup {
  public nationalActMinDate: Date;
  public singleCountrySelected = false;
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  public countryCheckboxOptions: any[] = [
    { id: 1, value: 'LUX', controlName: 'LUX', controlValue: false },
    { id: 2, value: 'ESP', controlName: 'ESP', controlValue: false },
    { id: 3, value: 'PRT', controlName: 'PRT', controlValue: false },
  ];

  public relations: string[] = [
    'eli:related_to',
    'eli:cites',
    'eli:transposes',
    'eli:ensures_implementation_of',
    'eli:applies',
  ];

  private static DATE_RANGE = 'range';
  private static DATE_SPECIFIC = 'specific';

  // control names
  private static NATIONAL_ACT_URI_CONTROL_NAME = 'NatActURI';
  private static DATE_RANGE_CONTROL_NAME = 'dateRange';
  private static DATE_TYPE_CONTROL_NAME = 'dateType';
  private static SPECIFIC_DATE_CONTROL_NAME = 'specificDate';
  private static DATE_FROM_CONTROL_NAME = 'dateFrom';
  private static DATE_TO_CONTROL_NAME = 'dateTo';
  private static EU_CRITERIA_TYPE_CONTROL_NAME = 'EUCriteriaType';
  private static EU_RELATION_CONTROL_NAME = 'relation';
  private static EU_ACT_URI_CONTROL_NAME = 'EUActURI';
  private static EU_CELEX_CONTROL_NAME = 'EUCelex';
  private static EU_YEAR_CONTROL_NAME = 'EUYear';

  constructor(initData?: SearchParams) {
    super({
      allCountries: new FormControl(initData ? initData.allCountries : true),
      countriesGroup: new FormGroup(
        {
          LUX: new FormControl(initData?.countriesGroup?.LUX),
          ESP: new FormControl(initData?.countriesGroup?.ESP),
          PRT: new FormControl(initData?.countriesGroup?.PRT),
        },
        requireCheckboxesToBeCheckedValidator(),
      ),
      NatActURI: new FormControl(initData?.NatActURI),
      dateRange: new FormControl(initData?.dateRange),
      dateType: new FormControl(initData?.dateType || ''),
      specificDate: new FormControl(initData?.specificDate),
      dateFrom: new FormControl(initData?.dateFrom),
      dateTo: new FormControl(initData?.dateTo),
      EUCriteriaType: new FormControl(initData?.EUCriteriaType),
      relation: new FormControl(initData?.relation || 'ANY'),
      EUActURI: new FormControl(initData?.EUActURI),
      EUCelex: new FormControl(initData?.EUCelex),
      EUYear: new FormControl(initData?.EUYear),
    });
  }

  buildPayload(): SearchParams {
    return {
      ...this.value,
      countries: this.buildCountryList(),
      relations: this.value.relation === 'ANY' ? this.relations : [this.value.relation],
    } as SearchParams;
  }

  reset() {
    super.reset();
    this.enable();
    // default value of the form
    this.patchValue({
      'allCountries': true,
      'dateType': '',
      'dateRange': undefined,
      'EUCriteriaType': undefined,
      'relation': 'ANY',
    });
  }

  public buildCountryList() {
    return Object.keys(this.countriesFormGroup.controls).filter(
      (key: string) => this.countriesFormGroup.controls[key].value,
    );
  }

  get countriesFormGroup(): FormGroup {
    return this.controls.countriesGroup as FormGroup;
  }

  patchAndSubscribe(value: SearchParams) {
    this.behaviourDependingOnAllCountriesValue$();
    this.patchValue(value, { emitEvent: false });
    this.updateSingleCountrySelected();
    this.behaviourDependingOnCountryValue$();
    this.behaviourDependingOnEUCriteriaType$();
    this.behaviourDependingOnNationalFromDateValue$();
    this.behaviourDependingOnNationalURI$();
    this.behaviourDependingOnNationalDates$();
    this.behaviourDependingOnNatDateType$();
    this.behaviourDependingOnNatDateRange$();
  }

  behaviourDependingOnCountryValue$() {
    this.controls.countriesGroup?.valueChanges
      .pipe(
        distinctUntilChanged(),
        map((value) => {
          if (Object.keys(value).every((key) => value[key] != null)) {
            this.controls.allCountries.setValue(
              Object.keys(value).every((key) => value[key]),
              { emitEvent: false },
            );
          }
          // show national identifier only for single country selection
          this.updateSingleCountrySelected();
        }),
      )
      .subscribe();
  }

  behaviourDependingOnAllCountriesValue$() {
    this.controls.allCountries?.valueChanges
      .pipe(
        startWith(this.controls.allCountries.value),
        map((value) => {
          // apply all countries value to individual countries
          Object.keys(this.countriesFormGroup.controls).forEach((key) => {
            this.countriesFormGroup.controls[key].setValue(value, { emitEvent: false });
          });

          // hide national identifier if all countries selected
          if (value) {
            this.singleCountrySelected = false;
            this.controls.NatActURI.setValue(undefined);
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnNatDateType$() {
    this.controls.dateType?.valueChanges
      .pipe(
        startWith(this.controls.dateType.value),
        distinctUntilChanged(),
        map((value) => {
          // disable specific date, date from and date to until a date type is selected
          if (value) {
            this.disableAndClearControls([
              EliSearchForm.EU_YEAR_CONTROL_NAME,
              EliSearchForm.EU_CELEX_CONTROL_NAME,
              EliSearchForm.EU_RELATION_CONTROL_NAME,
              EliSearchForm.EU_ACT_URI_CONTROL_NAME,
            ]);
            this.clearControls([
              EliSearchForm.NATIONAL_ACT_URI_CONTROL_NAME,
              EliSearchForm.EU_CRITERIA_TYPE_CONTROL_NAME,
            ]);
            this.controls.dateRange.enable({ emitEvent: false });
            this.controls.dateFrom.enable({ emitEvent: false });
            this.controls.dateTo.enable({ emitEvent: false });
            this.controls.specificDate.enable({ emitEvent: false });
          } else {
            this.controls.dateRange.disable({ emitEvent: false });
            this.controls.dateFrom.disable({ emitEvent: false });
            this.controls.dateFrom.clearValidators();
            this.controls.dateTo.disable({ emitEvent: false });
            this.controls.specificDate.disable({ emitEvent: false });
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnEUCriteriaType$() {
    this.controls.EUCriteriaType?.valueChanges
      .pipe(
        startWith(this.controls.EUCriteriaType.value),
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.controls.relation.enable({ emitEvent: false });
            if (value === 'eu-year') {
              this.disableAndClearControls([
                EliSearchForm.DATE_RANGE_CONTROL_NAME,
                EliSearchForm.SPECIFIC_DATE_CONTROL_NAME,
                EliSearchForm.DATE_FROM_CONTROL_NAME,
                EliSearchForm.DATE_TO_CONTROL_NAME,
                EliSearchForm.EU_CELEX_CONTROL_NAME,
                EliSearchForm.EU_ACT_URI_CONTROL_NAME,
              ]);
              this.clearControls([EliSearchForm.NATIONAL_ACT_URI_CONTROL_NAME, EliSearchForm.DATE_TYPE_CONTROL_NAME]);
              this.controls.EUYear.enable();
            } else if (value === 'eu-legalUri') {
              this.disableAndClearControls([
                EliSearchForm.DATE_RANGE_CONTROL_NAME,
                EliSearchForm.SPECIFIC_DATE_CONTROL_NAME,
                EliSearchForm.DATE_FROM_CONTROL_NAME,
                EliSearchForm.DATE_TO_CONTROL_NAME,
                EliSearchForm.EU_CELEX_CONTROL_NAME,
                EliSearchForm.EU_YEAR_CONTROL_NAME,
              ]);
              this.clearControls([EliSearchForm.NATIONAL_ACT_URI_CONTROL_NAME, EliSearchForm.DATE_TYPE_CONTROL_NAME]);
              this.controls.EUActURI.enable();
            } else if (value === 'eu-number') {
              this.disableAndClearControls([
                EliSearchForm.DATE_RANGE_CONTROL_NAME,
                EliSearchForm.SPECIFIC_DATE_CONTROL_NAME,
                EliSearchForm.DATE_FROM_CONTROL_NAME,
                EliSearchForm.DATE_TO_CONTROL_NAME,
                EliSearchForm.EU_YEAR_CONTROL_NAME,
                EliSearchForm.EU_ACT_URI_CONTROL_NAME,
              ]);
              this.clearControls([EliSearchForm.NATIONAL_ACT_URI_CONTROL_NAME, EliSearchForm.DATE_TYPE_CONTROL_NAME]);
              this.controls.EUCelex.enable();
            }
          } else {
            // no initial value, then disable EU inputs fields
            this.disableAndClearControls([
              EliSearchForm.EU_CELEX_CONTROL_NAME,
              EliSearchForm.EU_YEAR_CONTROL_NAME,
              EliSearchForm.EU_ACT_URI_CONTROL_NAME,
            ]);
            this.controls.EUYear.disable();
            this.controls.EUActURI.disable();
            this.controls.EUCelex.disable();
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnNationalFromDateValue$() {
    this.controls.dateFrom?.valueChanges
      .pipe(
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.nationalActMinDate = parseStringToDate(value);
            this.controls.dateTo?.setValidators(dateIsNotBeforeValueValidator(this.nationalActMinDate));
          } else {
            this.controls.dateTo?.setValidators([]);
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnNationalURI$() {
    this.controls.NatActURI?.valueChanges
      .pipe(
        debounceTime(500),
        startWith(this.controls.NatActURI.value),
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            // disable everything except national identifier and countries
            this.disableAndClearControls([
              EliSearchForm.DATE_RANGE_CONTROL_NAME,
              EliSearchForm.SPECIFIC_DATE_CONTROL_NAME,
              EliSearchForm.DATE_FROM_CONTROL_NAME,
              EliSearchForm.DATE_TO_CONTROL_NAME,
              EliSearchForm.EU_YEAR_CONTROL_NAME,
              EliSearchForm.EU_CELEX_CONTROL_NAME,
              EliSearchForm.EU_RELATION_CONTROL_NAME,
              EliSearchForm.EU_ACT_URI_CONTROL_NAME,
            ]);
            this.clearControl(EliSearchForm.DATE_TYPE_CONTROL_NAME);
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnNationalDates$() {
    this.controls.dateFrom?.valueChanges
      .pipe(
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.controls.dateRange.setValue(EliSearchForm.DATE_RANGE);
          }
        }),
      )
      .subscribe();

    this.controls.dateTo?.valueChanges
      .pipe(
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.controls.dateRange.setValue(EliSearchForm.DATE_RANGE);
          }
        }),
      )
      .subscribe();

    this.controls.specificDate?.valueChanges
      .pipe(
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.controls.dateRange.setValue(EliSearchForm.DATE_SPECIFIC);
          }
        }),
      )
      .subscribe();
  }

  behaviourDependingOnNatDateRange$() {
    this.controls.dateRange?.valueChanges
      .pipe(
        startWith(this.controls.dateRange.value),
        distinctUntilChanged(),
        map((value) => {
          // clear controls
          if (value === EliSearchForm.DATE_RANGE) {
            this.controls.specificDate.setValue(undefined);
            this.controls.dateFrom.addValidators(Validators.required);
            this.controls.dateTo.addValidators(Validators.required);
            this.controls.dateFrom.updateValueAndValidity({ emitEvent: false });
            this.controls.dateTo.updateValueAndValidity({ emitEvent: false });
          } else {
            // specific
            this.controls.dateFrom.clearValidators();
            this.controls.dateFrom.setValue(undefined);
            this.controls.dateFrom.clearValidators();
            this.controls.dateTo.setValue(undefined);
          }
        }),
      )
      .subscribe();
  }

  updateSingleCountrySelected() {
    this.singleCountrySelected =
      Object.keys(this.countriesFormGroup.value).filter((key) => !!this.countriesFormGroup.value[key]).length == 1;

    if (!this.singleCountrySelected) {
      this.controls.NatActURI.setValue(undefined);
    }
  }

  private disableAndClearControls(controlNames: string[]) {
    controlNames.forEach((controlName) => {
      this.controls[controlName].disable({ emitEvent: false });
      this.clearControl(controlName);
    });
  }

  private clearControls(controlNames: string[]) {
    controlNames.forEach((controlName) => {
      this.clearControl(controlName);
    });
  }

  private clearControl(controlName: string) {
    if (controlName === EliSearchForm.EU_RELATION_CONTROL_NAME) {
      this.controls[controlName].setValue('ANY');
    } else if (controlName === EliSearchForm.DATE_TYPE_CONTROL_NAME) {
      this.controls[controlName].setValue('');
    } else {
      this.controls[controlName].setValue(undefined);
    }
  }
}
