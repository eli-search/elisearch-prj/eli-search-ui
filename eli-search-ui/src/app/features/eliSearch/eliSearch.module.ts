import { NgModule } from '@angular/core';
import { EliSearchRoutingModule } from './eliSearch-routing.module';
import { EliSearchComponent } from './eliSearch.component';

import { EuiInputRadioModule } from '@eui/components/eui-input-radio';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiSelectModule } from '@eui/components/eui-select';
import { EuiDatepickerModule } from '@eui/components/eui-datepicker';
import { EuiFeedbackMessageModule } from '@eui/components/eui-feedback-message';
import { EuiPopoverModule } from '@eui/components/eui-popover';

import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    EliSearchRoutingModule,
    EuiInputRadioModule,
    EuiInputTextModule,
    EuiDatepickerModule,
    EuiSelectModule,
    EuiFeedbackMessageModule,
    EuiPopoverModule,
  ],
  declarations: [EliSearchComponent],
})
export class Module {}
