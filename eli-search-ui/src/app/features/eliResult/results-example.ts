export const oneCountry = {
  'head': {
    'vars': [
      'work',
      'date_publication',
      'first_date_entry_in_force',
      'date_no_longer_in_force',
      'title',
      'language',
      'country',
    ],
  },
  'results': {
    'bindings': [
      {
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/LUX',
        },
        'work': {
          'type': 'uri',
          'value': 'http://data.legilux.public.lu/eli/etat/leg/div/2024/05/14/a388/jo',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2024-09-03',
        },
        'title': {
          'type': 'literal',
          'value':
            'Accord entre le Gouvernement du Grand-Duché de Luxembourg et le Gouvernement de la République de Lituanie concernant l’échange et la protection réciproque d’informations classifiées, fait à Bruxelles, le 7 décembre 2020 - Entrée en vigueur.',
        },
      },
      {
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/LUX',
        },
        'work': {
          'type': 'uri',
          'value': 'http://data.legilux.public.lu/eli/etat/leg/loi/2024/08/27/a391/jo',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2024-09-04',
        },
        'title': {
          'type': 'literal',
          'value':
            'Loi du 27 août 2024 portant modification de la loi modifiée du 29 août 2008 sur la libre circulation des personnes et l’immigration.',
        },
      },
      {
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/LUX',
        },
        'work': {
          'type': 'uri',
          'value': 'http://data.legilux.public.lu/eli/etat/leg/rmin/2024/08/14/a390/jo',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2024-09-04',
        },
        'title': {
          'type': 'literal',
          'value':
            'Règlement ministériel du 14 août 2024 interdisant les évolutions d’aéronefs sans équipage à bord le 26 septembre 2024 à Luxembourg-Ville.',
        },
      },
      {
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/LUX',
        },
        'work': {
          'type': 'uri',
          'value': 'http://data.legilux.public.lu/eli/etat/leg/rgd/2024/08/27/a389/jo',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2024-09-04',
        },
        'title': {
          'type': 'literal',
          'value':
            'Règlement grand-ducal du 27 août 2024 portant adaptation pour certains lycées des articles 1er et 2 du règlement grand-ducal du 22 juillet 2024 fixant les grilles horaires, les coefficients des disciplines et des disciplines combinées, ainsi que les disciplines fondamentales des classes de l’enseignement secondaire classique.',
        },
      },
    ],
  },
};

export const allCountries = {
  'head': {
    'vars': [
      'work',
      'date_publication',
      'first_date_entry_in_force',
      'date_no_longer_in_force',
      'title',
      'language',
      'country',
    ],
  },
  'results': {
    'bindings': [
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/572/1997/07/30/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-30',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 572/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/574/1997/07/30/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-30',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 574/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/57/1995/01/25/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1995-01-25',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 57/95 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/575/1997/07/31/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-31',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 575/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/568/2002/06/05/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-05',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 568/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/571/1997/07/30/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-30',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 571/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/573/1997/07/30/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-30',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 573/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/567/2002/06/04/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-04',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 567/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/564/2002/06/04/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-04',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 564/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/565/2002/06/04/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-04',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 565/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/566/2002/06/04/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-04',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 566/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/570/1997/07/30/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '1997-07-30',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 570/97 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/586/2002/06/06/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-06',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 586/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/582/2002/06/05/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-05',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 582/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
      {
        'work': {
          'type': 'uri',
          'value': 'https://data.dre.pt/eli/port/583/2002/06/05/p/dre',
        },
        'country': {
          'type': 'uri',
          'value': 'http://publications.europa.eu/resource/authority/country/PRT',
        },
        'date_publication': {
          'datatype': 'http://www.w3.org/2001/XMLSchema#date',
          'type': 'literal',
          'value': '2002-06-05',
        },
        'title': {
          'type': 'literal',
          'value': 'Portaria n.º 583/2002 ',
        },
        'language': {
          'type': 'uri',
          'value': 'https://publications.europa.eu/resource/authority/language/PRT',
        },
      },
    ],
  },
};
