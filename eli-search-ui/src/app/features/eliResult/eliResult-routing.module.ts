import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EliResultComponent } from './eliResult.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: EliResultComponent }])],
})
export class EliResultRoutingModule {}
