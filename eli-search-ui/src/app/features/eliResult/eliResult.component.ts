import { Component, computed, OnInit, signal } from '@angular/core';
import { Router } from '@angular/router';
import { ResultService } from '../../services/result.service';
import { SearchService } from '../../services/search.service';
import { isDebug } from '@shared/helpers/environment.helpers';
import { SearchParams } from '@shared/models/search-params.model';
import { map, tap } from 'rxjs';
import { ELIS_DATE_FORMAT, formatDate } from '@shared/helpers/date.helpers';
import { TranslateService } from '@ngx-translate/core';
import { originalOrder } from '@shared/helpers/sort.helper';
import { EliResultForm } from './eliResult.form';
import { Constants } from '@shared/constants/constants';
import { EuiChip } from '@eui/components/eui-chip';
import { getFacetLabel } from '@shared/helpers/facet.helper';

@Component({
  templateUrl: './eliResult.component.html',
  styleUrls: ['./eliResult.component.scss'],
})
export class EliResultComponent implements OnInit {
  readonly originalOrder = originalOrder;

  /* eslint-disable  @typescript-eslint/no-explicit-any */
  results: any[];
  columns: string[];
  debug = isDebug();

  // infinite scroll properties
  $results = signal<any[]>([]);
  $filteredResults = signal<any[]>([]);
  $limit = signal<number>(Constants.SCROLL_DISPLAYED_ITEMS_SIZE);
  $displayedData = computed(() => {
    return this.groupDataMap(this.$filteredResults().slice(0, this.$limit()));
  });
  $facets = computed(() => {
    return this.generateFacets(this.$filteredResults().slice(0, this.$limit()));
  });
  $selectedFacets = signal<Set<ElisFacet>>(new Set());
  $isExpanded = signal<boolean>(false);

  executeSearch = true;
  endOfResults = false;
  limitOfResults = false;
  hasTertiaryLevel = true;

  filterText: string = undefined;

  // grouping properties
  $resultForm = signal<EliResultForm>(new EliResultForm());

  constructor(
    public searchService: SearchService,
    public resultService: ResultService,
    private router: Router,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.$resultForm().behaviourDependingOnValue$();
    this.displayData(this.resultService.getData());
    this.$resultForm()
      .valueChanges.pipe(
        tap(() => {
          if (this.$resultForm().valid && this.searchService.getSearchParams()) {
            console.log(this.searchService.getSearchParams());
            // trigger a new grouping
            if (this.debug) console.log('trigger a new search and do grouping');
            this.resultService.setData([]);
            this.displayData([]);
            this.searchService
              .setPageCurrent(0)
              .setLoading(true)
              .setPrimarySortingKey(
                this.$resultForm().getPrimarySortingKeyForSearch(this.searchService.getSearchParams().dateType),
              )
              .setSecondarySortingKey(
                this.$resultForm().getSecondarySortingKeyForSearch(this.searchService.getSearchParams().dateType),
              )
              .search()
              .pipe(
                map((data) => {
                  this.endOfResults = false;
                  this.searchService.setLoading(false);
                  this.resultService.setPrimarySortingKey(
                    this.$resultForm().getPrimarySortingKeyForResultGrouping(
                      this.searchService.getSearchParams().dateType,
                    ),
                  );
                  this.resultService.setSecondarySortingKey(
                    this.$resultForm().getSecondarySortingKeyForResultGrouping(
                      this.searchService.getSearchParams().dateType,
                    ),
                  );
                  this.resultService.setTertiarySortingKey(
                    this.$resultForm().getTertiarySortingKeyForResultGrouping(
                      this.searchService.getSearchParams().dateType,
                    ),
                  );
                  this.hasTertiaryLevel = !!this.resultService.getTertiarySortingKey();
                  this.resultService.setData(data);
                  this.resultService.setPagingData(this.searchService.getPagingData());
                  this.displayData(this.resultService.getData());
                }),
              )
              .subscribe();
          } else {
            console.log('sorting form not valid');
          }
        }),
      )
      .subscribe();
  }

  displayData(data: any) {
    if (data && data.results && data.results.bindings) {
      this.results = data.results.bindings;
      this.$results.set(data.results.bindings);
      this.$filteredResults.set(data.results.bindings);
      this.columns = data.head.vars;
    } else {
      this.results = [];
      this.$results.set([]);
      this.$filteredResults.set([]);
      this.columns = [];
    }

    this.limitOfResults = this.results.length >= Constants.MAX_RESULTS_SIZE;
  }

  // Group the data based on the primary and secondary grouping/sorting selected
  groupDataMap(data: any[]): Map<string, Map<string, any>> {
    const primarySortingKey = this.$resultForm().getPrimarySortingKeyForResultGrouping(
      this.searchService.getSearchParams().dateType,
    );
    const secondarySortingKey = this.$resultForm().getSecondarySortingKeyForResultGrouping(
      this.searchService.getSearchParams().dateType,
    );
    const tertiarySortingKey = this.$resultForm().getTertiarySortingKeyForResultGrouping(
      this.searchService.getSearchParams().dateType,
    );

    console.log(
      'grouping %i results by primary %o, secondary %o, tertiary %o',
      data.length,
      primarySortingKey,
      secondarySortingKey,
      tertiarySortingKey,
    );

    // level 1 grouping
    const groupingAccumulator = this.primaryGroupingAndSorting(data, primarySortingKey);
    [...groupingAccumulator.keys()].forEach((key) => {
      // level 2 grouping
      const secondarySortedMap = this.nestedGroupingAndSortingV2(key, groupingAccumulator, secondarySortingKey);
      if (tertiarySortingKey) {
        // level 3 grouping
        [...secondarySortedMap.keys()].forEach((key) => {
          this.nestedGroupingAndSortingV2(key, secondarySortedMap, tertiarySortingKey);
        });
      }
    });

    console.log('grouping result', groupingAccumulator);

    return groupingAccumulator;
  }

  private primaryGroupingAndSorting(data: any[], sortingKey: SortingKey) {
    // Primary grouping
    const grouping: Map<string, any> = data.reduce((acc: Map<string, any>, currentValue: any) => {
      // TODO dynamically pass 1 or nothing depending on the threshold to pass from one level to 2 levels for the date
      const key = this.getGroupKey(currentValue, sortingKey.field, sortingKey.fallbackField, sortingKey.level);
      const group = acc.get(key) || [];
      group.push(currentValue);
      acc.set(key, group);
      return acc;
    }, new Map<string, any>());

    if (this.debug) console.log('1st level grouping', grouping);

    // Convert to array of key-value pairs
    const primaryEntries = [...grouping.entries()];

    // Sort by keys
    primaryEntries.sort((a, b) => {
      if (sortingKey.sortOrder === 'asc') {
        return a[0].localeCompare(b[0]);
      }
      return b[0].localeCompare(a[0]);
    });

    // Create a new sorted Map
    return new Map(primaryEntries);
  }

  private nestedGroupingAndSortingV2(key: string, groupingAccumulator: Map<string, any>, sortingKey: SortingKey) {
    // sub grouping
    const secondaryGrouping: Map<string, any[]> = groupingAccumulator
      .get(key)
      .reduce((acc: Map<string, any>, currentValue: any) => {
        const key = this.getGroupKey(currentValue, sortingKey.field, sortingKey.fallbackField, sortingKey.level);
        const group = acc.get(key) || [];
        group.push(currentValue);
        acc.set(key, group);
        return acc;
      }, new Map<string, any>());

    // Convert to array of key-value pairs
    const secondaryEntries = [...secondaryGrouping.entries()];

    // Sort by keys
    secondaryEntries.sort((a, b) => {
      if (sortingKey.sortOrder === 'asc') {
        return a[0].localeCompare(b[0]);
      }
      return b[0].localeCompare(a[0]);
    });
    // Create a new sorted Map
    const secondarySortedMap = new Map(secondaryEntries);

    groupingAccumulator.set(key, secondarySortedMap);

    return secondarySortedMap;
  }

  // compute the grouping key
  private getGroupKey(item, field: string, fallbackField?: string, level?: number): string {
    const resultItem = item[field];
    if (resultItem && resultItem.value) {
      const resultItem = item[field];
      if (resultItem.datatype && resultItem.datatype === Constants.DATE_SCHEMA) {
        if (level == 1) {
          return formatDate(ELIS_DATE_FORMAT.display.year, resultItem.value);
        }
        if (level == 2) {
          return formatDate(ELIS_DATE_FORMAT.display.month, resultItem.value);
        }
        return formatDate(ELIS_DATE_FORMAT.display.yearMonth, resultItem.value);
      }
      if (resultItem.value.indexOf(Constants.COUNTRY_URI_PREFIX) >= 0) {
        return resultItem.value.replace(Constants.COUNTRY_URI_PREFIX, '');
      }
      if (resultItem.value.indexOf(Constants.RELATION_URI_PREFIX) >= 0) {
        return Constants.ELI_PREFIX + resultItem.value.replace(Constants.RELATION_URI_PREFIX, '');
      }
      return resultItem.value;
    } else if (fallbackField) {
      return this.getGroupKey(item, fallbackField, undefined, level);
    } else {
      return `-`;
    }
  }

  private isDateGrouping(field: string) {
    return field === Constants.DATE_PUBLICATION || field === Constants.SEARCH_DATE;
  }

  debugSearch() {
    this.router.navigate(['screen/sparqlTest']);
  }

  newSearch() {
    this.searchService.setSearchParams({} as SearchParams);
    this.router.navigate(['screen/eliSearch']);
  }

  modifySearch() {
    this.router.navigate(['screen/eliSearch']);
  }

  /**
   * When the user reaches the end of the page the next available results are rendered.
   * When the end of the batch is reached, a new search is triggered to fetch the next results.
   */
  /*onNearEndScroll(): void {
    if (this.executeSearch && !this.searchService.isLoading()) {
      // display the next results
      this.$limit.update((val) => val + Constants.SCROLL_DISPLAYED_ITEMS_SIZE);

      if (this.$limit() > Constants.MAX_RESULTS_SIZE) {
        // stop searching if we reach the max results size
        this.executeSearch = false;
      } else if (
        this.$limit() >= this.$results().length &&
        this.$results().length < this.searchService.getNumberOfResultsRequested()
      ) {
        console.log('All results already fetched and displayed.');
        this.executeSearch = false;
        this.endOfResults = true;
      } else if (this.$limit() >= this.$results().length) {
        // if we are at the end of the results, fetch the next batch
        this.searchService.setPageCurrent(this.searchService.getPageCurrent() + 1);
        this.searchService
          .setLoading(true)
          .search()
          .pipe(
            map((data) => {
              this.searchService.setLoading(false);
              if (data && data.results && data.results.bindings && data.results.bindings.length > 0) {
                const results = this.$results().concat(data.results.bindings);
                this.$results.set(results);
                if (this.debug) console.log('this.$results() length', this.$results().length);
              } else {
                console.log('End of the search. No more results');
                this.executeSearch = false;
                this.endOfResults = true;
              }
            }),
          )
          .subscribe();
      }
    }
  }*/

  /**
   * generate facets for some results fields (country, date and relation)
   * - reduce to group them by value (key = name, value = count)
   * - sort and keep TOP 5 occurrences
   *
   * @param data the results from the search
   */
  generateFacets(data: any[]) {
    const facets = new Map<string, Map<string, number>>();
    this.columns
      .filter((column) => {
        return [Constants.COUNTRY, Constants.DATE_PUBLICATION, Constants.RELATION].indexOf(column) >= 0;
      })
      .forEach((column) => {
        let fallbackColumn = undefined;
        if (column == Constants.DATE_PUBLICATION) {
          fallbackColumn = Constants.SEARCH_DATE;
        }
        const map: Map<string, number> = this.getFacetData(data, column, fallbackColumn);
        // only display facet when there's more than one choice
        if (Array.from(map.entries()).length > 1) {
          facets.set(column, map);
        }
      });
    console.log('generated facets', facets);
    return facets;
  }

  getFacetData(data: any[], column: string, fallbackColumn: string) {
    const facet: Map<string, any> = data.reduce((acc: Map<string, any>, currentValue: any) => {
      const key = this.getGroupKey(currentValue, column, fallbackColumn, 1);
      const count = acc.get(key) || 0;
      acc.set(key, count + 1);
      return acc;
    }, new Map<string, number>());

    // Convert the Map to an array of [key, value] entries and sort by occurrence descending
    // const sortedEntries = Array.from(facet.entries()).sort((a, b) => b[1] - a[1]);
    // Create a new Map from the sorted entries
    // const facetSorted = new Map<string, number>(sortedEntries);

    console.log('facet {}: ', column, facet);
    // console.log('facetSorted {}: ', column, facetSorted);

    return facet;
  }

  onFacetItemClickSelect(category, $event: any): void {
    this.onFacetItemClick(category, $event.target.value);
  }

  onFacetItemClick(category: string, value: string) {
    const elisFacet = {
      category,
      value,
      label: getFacetLabel(this.translateService, category, value),
    };
    console.log(elisFacet);
    // add to chip list (removable)
    if (![...this.$selectedFacets()].find((value1) => value1.value === elisFacet.value)) {
      this.$selectedFacets().add(elisFacet);
      this.applyFacetFiltering();
    }
  }

  applyFacetFiltering() {
    // reset filtered list to original results
    this.$filteredResults.set(this.$results());
    this.$isExpanded.set(false);

    if (this.$selectedFacets().size > 0) {
      // apply facet filtering of original results
      this.$filteredResults.set(
        this.$filteredResults().filter((item) => {
          let result = true;
          // 3 possible facet filtering
          // - country
          // - date
          // - relation
          this.$selectedFacets().forEach((facet) => {
            let fallbackField = undefined;
            if (facet.category == Constants.DATE_PUBLICATION) {
              fallbackField = Constants.SEARCH_DATE;
            }
            const itemValue = this.getGroupKey(item, facet.category, fallbackField, 1);
            if (facet.category == Constants.TITLE) {
              result = result && itemValue.toLowerCase().indexOf(facet.value.toLowerCase()) >= 0;
            } else {
              result = result && itemValue === facet.value;
            }
          });
          return result;
        }),
      );
    }
  }

  onFacetRemove(value: { chips: EuiChip[]; removed: any }) {
    if (value.removed) {
      for (const item of this.$selectedFacets()) {
        if (item.value === value.removed.id) {
          this.$selectedFacets().delete(item);
          break;
        }
      }
      // filter the results
      this.applyFacetFiltering();
    }
  }

  setExpanded(value: boolean) {
    this.$isExpanded.set(value);
  }

  onFilterTextEntered() {
    if (this.filterText && this.filterText.trim().length > 0) {
      const elisFacet = {
        category: Constants.TITLE,
        value: this.filterText,
        label: this.filterText,
      };
      this.$selectedFacets().add(elisFacet);
      // filter the results
      this.applyFacetFiltering();

      this.filterText = undefined;
    }
  }
}

export interface ElisFacet {
  category: string;
  value: string;
  label: string;
}

export interface SortingKey {
  field: string;
  fallbackField?: string;
  sortOrder: string;
  level?: number;
}
