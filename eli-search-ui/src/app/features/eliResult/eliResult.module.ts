import { NgModule } from '@angular/core';
import { EliResultRoutingModule } from './eliResult-routing.module';
import { EliResultComponent } from './eliResult.component';

import { SharedModule } from '@shared/shared.module';
import { EuiSelectModule } from '@eui/components/eui-select';
import { EuiTreeListModule } from '@eui/components/eui-tree-list';
import { EuiListModule } from '@eui/components/eui-list';
import { EuiInputTextModule } from '@eui/components/eui-input-text';
import { EuiInputGroupModule } from '@eui/components/eui-input-group';

@NgModule({
  imports: [
    SharedModule,
    EliResultRoutingModule,
    EuiSelectModule,
    EuiTreeListModule,
    EuiListModule,
    EuiInputTextModule,
    EuiInputGroupModule,
  ],
  declarations: [EliResultComponent],
})
export class Module {}
