import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GroupingModel } from '@shared/models/grouping.model';
import { SortingKey } from './eliResult.component';
import { Constants } from '@shared/constants/constants';
import { distinctUntilChanged, map, startWith } from 'rxjs';
import { differentSortCriteriaValidator } from '@shared/validators/different-sort-criteria.validator';

export class EliResultForm extends FormGroup {
  constructor(initData?: GroupingModel) {
    super({
      primary: new FormControl(initData?.primary || 'date_publication-desc', Validators.required),
      secondary: new FormControl(initData?.secondary || 'country-asc'),
    });
  }

  behaviourDependingOnValue$() {
    this.controls.primary?.valueChanges
      .pipe(
        startWith(this.controls.primary?.value),
        distinctUntilChanged(),
        map((value) => {
          if (value) {
            this.controls.secondary?.setValidators([Validators.required, differentSortCriteriaValidator(value)]);
            this.controls.secondary?.updateValueAndValidity();
          }
        }),
      )
      .subscribe();
  }

  public getPrimarySortingKeyForSearch(dateType: string) {
    return this.getSortingKeys(this.controls.primary?.value, dateType);
  }

  public getSecondarySortingKeyForSearch(dateType: string) {
    return this.getSortingKeys(this.controls.secondary?.value, dateType);
  }

  public getPrimarySortingKeyForResultGrouping(dateType: string) {
    return this.getSortingKeys(this.controls.primary?.value, dateType, 1);
  }

  public getSecondarySortingKeyForResultGrouping(dateType: string) {
    if (this.controls.primary?.value.startsWith(Constants.DATE_PUBLICATION)) {
      return this.getSortingKeys(this.controls.primary?.value, dateType, 2);
    }
    return this.getSortingKeys(this.controls.secondary?.value, dateType, 1);
  }

  public getTertiarySortingKeyForResultGrouping(dateType: string) {
    if (this.controls.primary?.value.startsWith(Constants.DATE_PUBLICATION)) {
      return this.getSortingKeys(this.controls.secondary?.value, dateType, 2);
    }
    if (this.controls.secondary?.value.startsWith(Constants.DATE_PUBLICATION)) {
      return this.getSortingKeys(this.controls.secondary?.value, dateType, 2);
    }
    return undefined;
  }

  private getSortingKeys(criteria: string, dateType: string, level?: number): SortingKey {
    const splitted = criteria.split('-');
    const sortingKey = {
      field: splitted[0],
      sortOrder: splitted[1],
      level,
    } as SortingKey;

    if (sortingKey.field === Constants.DATE_PUBLICATION) {
      sortingKey.fallbackField = Constants.SEARCH_DATE;
      if (dateType) {
        // for date search we use search_date property as main field
        sortingKey.field = Constants.SEARCH_DATE;
        sortingKey.fallbackField = Constants.DATE_PUBLICATION;
      }
    }

    return sortingKey;
  }
}
