import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../../services/configuration.service';
import { ConfigParams } from '../../shared/models/config-params.model';

@Component({
  templateUrl: './set-config.component.html',
  styleUrl: './set-config.component.scss',
})
export class SetConfigComponent implements OnInit {
  configParams: ConfigParams = {
    sparqlEndpoint: '',
    defaultQuery: '',
    cellarFederatedQuery: '',
    yearQuery: '',
    euLegalActQuery: '',
    relationshipQuery: '',
    euLegalActYearRelationshipQuery: '',
    euCelexRelationshipQuery: '',
    nationalUriQuery: '',
  };

  constructor(private configService: ConfigurationService) {}

  ngOnInit() {
    this.configParams = this.configService.getConfig();
  }

  onSubmit() {
    this.configService.updateConfig(this.configParams);
    alert('Configuration updated successfully!');
  }
}
