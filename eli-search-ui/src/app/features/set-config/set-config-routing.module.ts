import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SetConfigComponent } from './set-config.component';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: SetConfigComponent }])],
})
export class SetConfigRoutingModule {}
