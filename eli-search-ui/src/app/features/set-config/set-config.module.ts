import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { SetConfigRoutingModule } from './set-config-routing.module';
import { SetConfigComponent } from './set-config.component';

@NgModule({
  declarations: [SetConfigComponent],
  imports: [SetConfigRoutingModule, SharedModule],
})
export class Module {}
