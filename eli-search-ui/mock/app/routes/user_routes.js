const User = require('../models/user');
const uuid = require('uuid');
const { createProxyMiddleware } = require('http-proxy-middleware');
const axios = require('axios');

module.exports = function (app, db) {
  db.then((db) => {
    /*
        app.use('/api/sparql', createProxyMiddleware({
            target: 'https://publications.europa.eu',
            changeOrigin: true,
            secure: false,
            pathRewrite: {
                '^/api/sparql' : '/webapi/rdf/sparql'
            },
            onProxyReq: (proxyReq, req, res) => {
                // Modify the request headers if needed
            },
            onError: (err, req, res) => {
                res.status(500).send('Proxy error');
            },
            onProxyRes: (proxyRes, req, res) => {
                // Modify the response from the remote API if needed
            }
        }));
        */

    app.get('/api/sparql', async (req, res) => {
      try {
        const response = await axios.get('https://publications.europa.eu/webapi/rdf/sparql', {
          params: req.query,
          headers: {},
        });
        res.json(response.data);
      } catch (error) {
        res.status(500).send('Proxy error');
      }
    });

    app.get('/api/user-details', (req, res) => {
      res.send(db.get('user-details'));
    });

    app.get('/api/users', (req, res) => {
      res.send(db.get('users'));
    });

    app.get('/api/users/:userId', (req, res) => {
      res.send(db.get('users').find({ userId: req.params.userId }));
    });

    app.put('/api/users/:userId', (req, res) => {
      db.get('users')
        .find({ userId: req.params.userId })
        .assign({ ...req.body })
        .write()
        .then((user) => res.send(user));
    });

    app.delete('/api/users/:userId', (req, res) => {
      db.get('users').remove({ userId: req.params.userId }).write().then(res.send());
    });

    app.post('/api/users', (req, res) => {
      db.get('users')
        .push({ ...User, ...req.body, ...{ userId: uuid.v4() } })
        .last()
        .write()
        .then((user) => res.send(user));
    });
  });
};
