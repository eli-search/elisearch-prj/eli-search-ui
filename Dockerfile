# Multi-stage DockerFile :
# - Build & Test using bookworm-slim and chrome headless (yarn dependency)
# - Serve using alpine & nginx

# Stage 0, build and test using node
FROM node:22-bookworm-slim AS build

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    gcc build-essential wget gnupg2 ca-certificates

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor | tee /etc/apt/trusted.gpg.d/google.gpg \
    && chmod a+r /etc/apt/trusted.gpg.d/google.gpg \
    && sh -c 'echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/trusted.gpg.d/google.gpg] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'


RUN apt-get update \
    && apt --fix-broken install \
    && apt-get install -y --no-install-recommends google-chrome-stable


RUN mkdir /workdir
COPY ./eli-search-ui /workdir
WORKDIR /workdir

RUN yarn install --ignore-engines

RUN yarn build-prod-skip-test

## Stage 1, Serve compiled pages with nginx
#FROM nginx:alpine AS serve
#
#COPY --from=build /workdir/dist/browser /usr/share/nginx/html
#COPY ./misc/nginx_default.conf /etc/nginx/nginx.conf
#
#RUN ln -sf /dev/stdout /var/log/nginx/access.log \
#    && ln -sf /dev/stderr /var/log/nginx/error.log
#
#EXPOSE 80
#STOPSIGNAL SIGTERM
#CMD ["/usr/sbin/nginx", "-g", "daemon off;"]

FROM python:3.11-slim-bookworm AS host

ARG ENV="dev"
ENV ENV=${ENV} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.4.1


RUN apt update && apt install -y python3-pip \
    && pip3 install "poetry==$POETRY_VERSION" \
    && apt remove -y python3-pip \
    && apt autoremove --purge -y \
    && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/*.list

WORKDIR /app
COPY ./eli-search-api/poetry.lock ./eli-search-api/pyproject.toml /app/
COPY ./eli-search-api /app
RUN poetry config virtualenvs.create false && \
    poetry install $(test $ENV == "prod" && echo "--no-dev") --no-interaction --no-ansi

COPY --from=build /workdir/dist/browser /app/static

ENTRYPOINT ["poetry", "run", "api"]
